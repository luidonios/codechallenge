package com.chargeanywhere.louiedonios.codechallenge.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPerson(Person person);

    @Query("SELECT * FROM person ORDER BY id ASC")
    LiveData<List<Person>> getPersonsList();

    @Query("SELECT * FROM person ORDER BY name ASC")
    LiveData<List<Person>> getPersonsListOrderByName();

    @Query("SELECT * FROM person ORDER BY date ASC")
    LiveData<List<Person>> getPersonsListOrderByDate();

    @Query("SELECT * FROM person WHERE id= :id")
    LiveData<Person> getPersonById(long id);
}
