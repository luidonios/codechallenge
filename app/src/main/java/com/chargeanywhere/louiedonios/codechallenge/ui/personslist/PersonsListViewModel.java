package com.chargeanywhere.louiedonios.codechallenge.ui.personslist;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.repository.PersonRepository;
import com.chargeanywhere.louiedonios.codechallenge.ui.Constants;

import java.util.Date;
import java.util.List;

public class PersonsListViewModel extends ViewModel {

    private PersonRepository personsRepository;

    private MutableLiveData<Integer> sortByOptionLiveData = new MutableLiveData<>();

    LiveData<List<Person>> sortedPersonsLiveData = Transformations.switchMap(sortByOptionLiveData, option -> {
        return getPersonsListSortedBy(option);
    });


    public PersonsListViewModel(PersonRepository personsRepository) {
        this.personsRepository = personsRepository;
    }

    public void insertPerson(String name, Date date) {
        final Person person = new Person();
        person.name = name;
        person.date = date;
        personsRepository.insertPerson(person);
    }

    public LiveData<List<Person>> getPersonsList() {
        int option = sortByOptionLiveData.getValue() != null ? sortByOptionLiveData.getValue() : 0;

        return getPersonsListSortedBy(option);
    }

    private LiveData<List<Person>> getPersonsListSortedBy(int option) {
        switch (option){
            case Constants.SORT_BY_ID:
                return personsRepository.getPersonsList();
            case Constants.SORT_BY_NAME:
                return personsRepository.getPersonsListOrderByName();

            case Constants.SORT_BY_DATE:
                return personsRepository.getPersonsListOrderByDate();
        }

        return null;
    }

    public void sortPersonsListBy(int option) {
        sortByOptionLiveData.setValue(option);
    }

}
