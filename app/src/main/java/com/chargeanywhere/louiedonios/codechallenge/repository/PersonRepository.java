package com.chargeanywhere.louiedonios.codechallenge.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.chargeanywhere.louiedonios.codechallenge.db.AppDatabase;
import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.db.PersonDao;

import java.util.List;

public class PersonRepository {

    private PersonDao personDao;

    public PersonRepository(PersonDao personDao) {
        this.personDao = personDao;
    }

    public void insertPerson(Person person) {
        personDao.insertPerson(person);
    }

    public LiveData<List<Person>> getPersonsList() {
        return personDao.getPersonsList();
    }

    public LiveData<List<Person>> getPersonsListOrderByName() {
        return personDao.getPersonsListOrderByName();
    }

    public LiveData<List<Person>> getPersonsListOrderByDate() {
        return personDao.getPersonsListOrderByDate();
    }

    public LiveData<Person> getPersonById(long id) {
        return personDao.getPersonById(id);
    }

    public static class Factory {

        public static PersonRepository make(Context context) {
            final AppDatabase appDatabase =
                    AppDatabase.Factory.make(context);
            final PersonDao personDao = appDatabase.getPersonDao();
            return new PersonRepository(personDao);
        }
    }
}
