package com.chargeanywhere.louiedonios.codechallenge.ui.persondetails;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.repository.PersonRepository;

public class PersonDetailsViewModel extends ViewModel {

    public static final String PERSON_ID = "person_id";

    private SavedStateHandle savedStateHandle;
    private PersonRepository personRepository;

    private MutableLiveData<Long> personIdLiveData = new MutableLiveData<>();
    public LiveData<Person> personLiveData = Transformations.switchMap(personIdLiveData, personId -> {
        return personRepository.getPersonById(personId);
    });

    public PersonDetailsViewModel(SavedStateHandle savedStateHandle,
                                  PersonRepository personRepository) {
        this.savedStateHandle = savedStateHandle;
        this.personRepository = personRepository;
        final long personId = savedStateHandle.get(PERSON_ID);
        personIdLiveData.setValue(personId);
    }
}
