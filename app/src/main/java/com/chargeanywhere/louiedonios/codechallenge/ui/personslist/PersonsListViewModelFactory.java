package com.chargeanywhere.louiedonios.codechallenge.ui.personslist;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.chargeanywhere.louiedonios.codechallenge.repository.PersonRepository;

public class PersonsListViewModelFactory implements ViewModelProvider.Factory {

    private final Context context;

    public PersonsListViewModelFactory(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        final PersonRepository personRepository =
                PersonRepository.Factory.make(context);
        return (T) new PersonsListViewModel(personRepository);
    }
}
