package com.chargeanywhere.louiedonios.codechallenge.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class LocalService extends Service {

    private static final int MINUTE = 1000 * 60;

    private final Binder binder = new LocalServiceBinder();
    private final IPersonDataFactory personDataFactory = new PersonDataFactory();

    private Handler handler;
    private RunnablePersonDataGenerator runnablePersonDataGenerator;
    private OnDataReadyListener onDataReadyListener;


    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler();
        runnablePersonDataGenerator =
                new RunnablePersonDataGenerator(this);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        handler.removeCallbacks(runnablePersonDataGenerator);
        runnablePersonDataGenerator = null;
        handler = null;
        onDataReadyListener = null;

        stopSelf();
    }

    public void setOnDataReadyListener(OnDataReadyListener onDataReadyListener) {
        this.onDataReadyListener = onDataReadyListener;
    }

    public void getPersonsList() {
        handler.postDelayed(runnablePersonDataGenerator, MINUTE);
    }

    public class LocalServiceBinder extends Binder {

        public LocalService getService() {
            return LocalService.this;
        }

    }

    private static class RunnablePersonDataGenerator implements Runnable {

        private final LocalService localService;
        public RunnablePersonDataGenerator(LocalService dataSorceService) {
            this.localService = dataSorceService;
        }

        @Override
        public void run() {

            final PersonData personData = localService.personDataFactory.make();

            if (localService.onDataReadyListener!= null) {

                localService.onDataReadyListener.onDataReady(personData);

                if(localService.handler != null) {
                    localService.handler.postDelayed(this, LocalService.MINUTE);
                }
            }
        }
    }

    public interface OnDataReadyListener {
        void onDataReady(PersonData data);
    }


}
