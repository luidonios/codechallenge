package com.chargeanywhere.louiedonios.codechallenge.ui.personslist;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.chargeanywhere.louiedonios.codechallenge.R;
import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.service.LocalService;
import com.chargeanywhere.louiedonios.codechallenge.service.PersonData;
import com.chargeanywhere.louiedonios.codechallenge.ui.persondetails.PersonDetailsViewModel;

import java.util.List;

public class PersonsListFragment extends Fragment implements LocalService.OnDataReadyListener,
        AdapterView.OnItemSelectedListener, PersonsListAdapter.OnItemClickListener {

    private LocalService localService;
    private boolean isBound;
    private PersonsListViewModelFactory personsListViewModelFactory;
    private PersonsListViewModel personsListViewModel;
    private PersonsListAdapter personsListAdapter;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalService.LocalServiceBinder binder = (LocalService.LocalServiceBinder) service;
            localService = binder.getService();
            localService.setOnDataReadyListener(PersonsListFragment.this::onDataReady);

            localService.getPersonsList();

            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            localService.setOnDataReadyListener(null);
            localService = null;
            isBound = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupPersonsListViewModel();
        setupPersonsListObserver();
        setupSortedPersonsObserver();
    }

    private void setupSortedPersonsObserver() {
        if (personsListViewModel != null &&
                personsListViewModel.sortedPersonsLiveData != null) {
            personsListViewModel.sortedPersonsLiveData.observe(this, new Observer<List<Person>>() {
                @Override
                public void onChanged(List<Person> personList) {
                    if (personsListAdapter != null) {
                        //Clear list to show new sorted list
                        personsListAdapter.submitList(null);
                        personsListAdapter.submitList(personList);
                    }
                }
            });

        }
    }

    private void setupPersonsListViewModel() {
        personsListViewModelFactory = new PersonsListViewModelFactory(requireContext());
        personsListViewModel =
                new ViewModelProvider(this, personsListViewModelFactory).get(PersonsListViewModel.class);
    }

    private void setupPersonsListObserver() {
        personsListViewModel.getPersonsList().observe(this, new Observer<List<Person>>() {
            @Override
            public void onChanged(List<Person> personList) {
                if (personsListAdapter != null) {
                    personsListAdapter.submitList(personList);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_persons_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupSpinnerSortBy();
        setupRVPersons();
    }

    private void setupSpinnerSortBy() {
        final Spinner spinnerSortBy = getView().findViewById(R.id.spinner_sort_by);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(requireContext(),
                        R.array.sort_by_options, android.R.layout.simple_spinner_dropdown_item);

        spinnerSortBy.setAdapter(adapter);
        spinnerSortBy.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if(personsListViewModel != null) {
            personsListViewModel.sortPersonsListBy(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //Do Nothing
    }

    private void setupRVPersons() {
        final RecyclerView rvPersons = getView().findViewById(R.id.rv_persons);

        personsListAdapter = new PersonsListAdapter(this);
        rvPersons.setAdapter(personsListAdapter);

        final LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
        rvPersons.setLayoutManager(linearLayoutManager);
        rvPersons.addItemDecoration(new DividerItemDecoration(requireContext(),
                linearLayoutManager.getOrientation()));
    }

    @Override
    public void onItemClick(View view, long personId) {
        Bundle bundle = new Bundle();
        bundle.putLong(PersonDetailsViewModel.PERSON_ID, personId);
        Navigation.findNavController(view).navigate(R.id.action_personsListFragment_to_personDetailsFragment, bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        unBindService();
    }

    private void unBindService() {
        if (isBound) {
            requireActivity().unbindService(serviceConnection);
            isBound = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        startService();
    }

    private void startService() {
        //start service so it can still perform action,
        // even there is no activity/fragment binded to it.
        Intent serviceIntent = new Intent(requireContext(), LocalService.class);
        requireActivity().startService(serviceIntent);

        bindService();
    }

    private void bindService() {
        Intent serviceIntent =
                new Intent(requireContext(), LocalService.class);

        requireActivity().bindService(serviceIntent,
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDataReady(PersonData data) {
        if (isBound) {

            if (personsListViewModel != null) {
                personsListViewModel.insertPerson(
                        data.getName(),
                        data.getDate());
            }
        }
    }
}