package com.chargeanywhere.louiedonios.codechallenge.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity(tableName = "person")
public class Person {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    public Date date;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                Objects.equals(name, person.name) &&
                Objects.equals(date, person.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, date);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                '}';
    }

    public static class Utils {

        public static String dateToStringWithPattern(Date date, String pattern) {
            if (date == null || (null == pattern && !pattern.isEmpty())) {
                return null;
            }
            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat(pattern);

            return simpleDateFormat.format(date);
        }
    }
}
