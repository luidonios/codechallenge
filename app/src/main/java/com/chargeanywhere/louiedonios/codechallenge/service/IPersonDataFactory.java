package com.chargeanywhere.louiedonios.codechallenge.service;

public interface IPersonDataFactory {
    PersonData make();
}
