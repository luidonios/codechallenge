package com.chargeanywhere.louiedonios.codechallenge.service;

import java.util.Date;

public class PersonData {

    private String name;
    private Date date;

    public PersonData(String name, Date date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date data) {
        this.date = data;
    }

    @Override
    public String toString() {
        return "PersonData{" +
                "name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
}
