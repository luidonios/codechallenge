package com.chargeanywhere.louiedonios.codechallenge.service;

import com.github.javafaker.Faker;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PersonDataFactory implements IPersonDataFactory {
    private Faker faker = new Faker();

    public PersonData make() {
        String randomName = faker.name().name();
        long randomTime = faker.date().birthday().getTime();

        return new PersonData(randomName, new Date(randomTime));
    }
}
