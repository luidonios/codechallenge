package com.chargeanywhere.louiedonios.codechallenge.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Person.class}, version = 1)
@TypeConverters(DateTypeConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PersonDao getPersonDao();

    public static class Factory {

        private static final String DB_NAME = "app-database";

        public static AppDatabase make(Context context) {

            return Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, DB_NAME)
                    .allowMainThreadQueries().build();
        }
    }
}
