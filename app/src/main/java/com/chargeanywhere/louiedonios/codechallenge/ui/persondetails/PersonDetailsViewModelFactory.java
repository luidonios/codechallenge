package com.chargeanywhere.louiedonios.codechallenge.ui.persondetails;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import androidx.savedstate.SavedStateRegistryOwner;

import com.chargeanywhere.louiedonios.codechallenge.repository.PersonRepository;

public class PersonDetailsViewModelFactory extends AbstractSavedStateViewModelFactory {

    private Context context;

    public PersonDetailsViewModelFactory(Context context, SavedStateRegistryOwner owner, Bundle bundle) {
        super(owner, bundle);
        this.context = context;
    }

    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass,
                                             @NonNull  SavedStateHandle handle) {
        final PersonRepository personRepository =
                PersonRepository.Factory.make(context);
        return (T) new PersonDetailsViewModel(handle, personRepository);
    }
}
