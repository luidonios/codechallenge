package com.chargeanywhere.louiedonios.codechallenge.ui.personslist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.chargeanywhere.louiedonios.codechallenge.R;
import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.ui.Constants;

public class PersonsListAdapter extends ListAdapter<Person, PersonsListAdapter.ViewHolder> {

    private OnItemClickListener onItemClickListener;
    public PersonsListAdapter(OnItemClickListener onItemClickListener) {
        super(DIFF_UTIL);
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.view_holder_persons_list, parent, false);
        final PersonsListAdapter.ViewHolder viewHolder =
                new PersonsListAdapter.ViewHolder(itemView);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    final Person personData = getItem(viewHolder.getAdapterPosition());
                    onItemClickListener.onItemClick(v, personData.id);
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonsListAdapter.ViewHolder holder, int position) {
        final Person person = getItem(position);
        holder.bindData(person);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvId;
        private final TextView tvName;
        private final TextView tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tv_id);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_date);
        }

        public void bindData(Person data) {
            tvId.setText("id: "+data.id);
            tvName.setText("name: "+data.name);
            tvDate.setText("date: " + Person.Utils.dateToStringWithPattern(data.date, Constants.DATE_FORMAT));
        }
    }

    public static final DiffUtil.ItemCallback<Person> DIFF_UTIL = new DiffUtil.ItemCallback<Person>() {

        @Override
        public boolean areItemsTheSame(@NonNull Person oldItem, @NonNull Person newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Person oldItem, @NonNull Person newItem) {
            return oldItem.equals(newItem);
        }
    };

    public interface OnItemClickListener {
        public void onItemClick(View v, long personId);
    }

}
