package com.chargeanywhere.louiedonios.codechallenge.ui;

public class Constants {

    public static final String DATE_FORMAT = "dd-M-yyyy hh:mm:ss";

    public static final int SORT_BY_ID = 0;
    public static final int SORT_BY_NAME = 1;
    public static final int SORT_BY_DATE = 2;

}
