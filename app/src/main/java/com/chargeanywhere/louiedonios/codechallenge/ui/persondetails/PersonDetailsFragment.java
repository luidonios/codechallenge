package com.chargeanywhere.louiedonios.codechallenge.ui.persondetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.chargeanywhere.louiedonios.codechallenge.R;
import com.chargeanywhere.louiedonios.codechallenge.db.Person;
import com.chargeanywhere.louiedonios.codechallenge.ui.Constants;
import com.chargeanywhere.louiedonios.codechallenge.ui.persondetails.PersonDetailsViewModel;
import com.chargeanywhere.louiedonios.codechallenge.ui.persondetails.PersonDetailsViewModelFactory;

public class PersonDetailsFragment extends Fragment {

    private PersonDetailsViewModelFactory personDetailsViewModelFactory;
    private PersonDetailsViewModel personDetailsViewModel;

    private TextView tvId;
    private TextView tvName;
    private TextView tvDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        personDetailsViewModelFactory =
                new PersonDetailsViewModelFactory(requireContext(),this, getArguments());
        personDetailsViewModel =
                new ViewModelProvider(this, personDetailsViewModelFactory).get(PersonDetailsViewModel.class);

        personDetailsViewModel.personLiveData.observe(this, new Observer<Person>() {
            @Override
            public void onChanged(Person person) {
                tvId.setText("id: "+person.id);
                tvName.setText("name: "+person.name);
                tvDate.setText("date: "+Person.Utils.dateToStringWithPattern(person.date, Constants.DATE_FORMAT));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvId = view.findViewById(R.id.tv_id);
        tvName = view.findViewById(R.id.tv_name);
        tvDate = view.findViewById(R.id.tv_date);
    }
}